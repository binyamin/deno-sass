# Building

If somebody can automate this, please let me know.

1. Download the sass script from the ESM.sh CDN.
   - The direct URL is `https://esm.sh/v99/sass/deno/sass.development.js`. The
     development version is easier to edit. You should use the latest version:
     For example, `/v99/sass@1.56.1/`.
   - ESM.sh replaces references to Node.js built-ins with Deno's
     node-compatibility modules.
2. Make the necessary changes:
   1. **Rebase URLs** - All imports should start with `https://`. Do a search
      for `/v99/...`, and replace it with `https://esm.sh/v99/...`.
   2. **Import for Production** - All ESM.sh imports should use the production
      version. Do a search for `.development.js`, and
      remove `.development`.
   3. **Polyfill `location.href`** - Deno doesn't fill the global `location`
      object by default:
	```js
	// Import this at the top
	import * as denoPath from 'https://deno.land/std@0.166.0/path/mod.ts';
	// Around line 970...
	Primitives_currentUri() {
		if (!!self.location)
			return self.location.href;
		// Replace this line...
		return null;
		// With this line...
		return denoPath.toFileUrl(Deno.cwd() + '/').toString() || null;
	}
	```
   4. **Other Polyfills**: Around lines 55 and 58
   ```js
   // Replace these
   self.__dirname = 'https://esm.sh/v99/sass@1.56.1/deno';
   self.__filename = 'https://esm.sh/v99/sass@1.56.1/deno/sass.development.js';

   // With these
   self.__dirname = denoPath.dirname(denoPath.fromFileUrl(import.meta.url));
   self.__filename = denoPath.fromFileUrl(import.meta.url);
   ```
3. Save it as `./out/sass.js`.
4. Run `scripts/build.ts` to minify the script.
