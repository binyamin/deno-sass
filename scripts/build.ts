import { esbuild, prettyBytes, resolvePath } from './deps.ts';

const r = await esbuild.build({
	entryPoints: [resolvePath('out/sass.js')],
	minify: true,
	platform: 'browser',
	format: 'esm',
	color: !Deno.noColor,
	target: 'esnext',
	outfile: resolvePath('out/sass.min.js'),
	write: true,
	logLevel: 'info',
	metafile: true,
});

console.log(
	'Input: %s',
	prettyBytes(r.metafile.inputs['out/sass.js'].bytes),
);
console.log(
	'Output: %s',
	prettyBytes(r.metafile.outputs['out/sass.min.js'].bytes),
);

esbuild.stop();
