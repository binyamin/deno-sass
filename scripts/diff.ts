import { prettyBytes, resolvePath } from './deps.ts';

function fileDiff(after: string, before: string) {
	const curr = Deno.statSync(resolvePath(after)).size;
	const prev = Deno.statSync(resolvePath(before)).size;
	return prettyBytes(curr - prev, { signed: true });
}

const raw = fileDiff('out/sass.js', 'out/1.55.0/sass.js');
const min = fileDiff('out/sass.min.js', 'out/1.55.0/sass.min.js');

console.group('Diff');
console.log('Input: %s', raw);
console.log('Output: %s', min);
console.groupEnd();
