# Sass for Deno

A sass module for Deno. Just [dart-sass](https://github.com/sass/dart-sass),
with some minor modifications, so it's deno-compatible.

## Usage

This module tries to mimic the behavior of
[the Dart-Sass NPM package](https://www.npmjs.com/package/sass)
([docs](https://sass-lang.com/documentation/js-api/)). If it doesn't, consider
it a bug.

## Contributing

All input is welcome; feel free to
[open an issue](https://gitlab.com/binyamin/deno-sass/-/issues/new). Please
remember to be a [mensch](https://www.merriam-webster.com/dictionary/mensch). If
you want to program, you can browse
[the issue list](https://gitlab.com/binyamin/deno-sass/-/issues).

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/deno-sass/-/blob/main/LICENSE).
Copyright 2022 Binyamin Aron Green.
